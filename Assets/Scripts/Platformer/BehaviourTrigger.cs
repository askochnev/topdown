﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTrigger : MonoBehaviour
{
    public EnemyBehaviour behaviourType = EnemyBehaviour.MoveLeft;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var enemy = other.GetComponent<EnemyCharacterController>();
        if (enemy != null)
        {
            switch (behaviourType)
            {
                case EnemyBehaviour.MoveLeft:
                    enemy.BeginMoveLeft();
                    break;
                case EnemyBehaviour.MoveRight:
                    enemy.BeginMoveRight();
                    break;
                case EnemyBehaviour.Jump:
                    enemy.Jump();
                    break;
            }
        }
    }
}
