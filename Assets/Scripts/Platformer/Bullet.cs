﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Vector2 _direction;
    
    [SerializeField] 
    private Rigidbody2D _rb;

    [Range(1,10)] 
    public float velocity;
    public void SetDirection(Vector2 dir)
    {
        _direction = dir;
    }

    void FixedUpdate()
    {
        _rb.velocity = _direction; 
    }
}
