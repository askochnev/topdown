﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField]
    private JumpHelper _jumpHelper;

    [SerializeField]
    private Rigidbody2D _rb;

    [SerializeField]
    private Gun _gun;

    [Range(1, 10)]
    public float velocity = 5;

    [Range(10, 25)]
    public float jumpForce = 17;

    public void Stop()
    {
        var vel = _rb.velocity;
        vel.x = 0;
        _rb.velocity = vel;
    }

    public void MoveLeft()
    {        
        var vel = _rb.velocity;
        vel.x = -velocity;
        _rb.velocity = vel;
        _gun.AimLeft();
    }
    public void MoveRight()
    {
        var vel = _rb.velocity;
        vel.x = velocity;
        _rb.velocity = vel;
        _gun.AimRight();
    }
    public void Jump()
    {
        if(_jumpHelper.MayJump())
        {
            var vel = _rb.velocity;
            vel.y =jumpForce;
            _rb.velocity = vel;
            _jumpHelper.OnJump();
        }
    }
    public void Shoot()
    {
        _gun.Shoot();
    }
}
