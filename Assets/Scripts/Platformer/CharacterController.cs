﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterController : MonoBehaviour
{
    [SerializeField]
    protected Character controlledTarget;

    private void Awake()
    {
        if(controlledTarget == null)
        {
            controlledTarget = GetComponent<Character>();
        }
    }
    private void FixedUpdate() 
    {
        ProcessInput(controlledTarget);
    }
    public abstract void ProcessInput(Character target);
}
