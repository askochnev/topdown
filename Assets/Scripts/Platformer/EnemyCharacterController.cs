﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharacterController : CharacterController
{
    public EnemyBehaviour behaviour = EnemyBehaviour.MoveLeft;
    public override void ProcessInput(Character target)
    {
        switch(behaviour)
        {
            case EnemyBehaviour.MoveLeft:
                target.MoveLeft();
                break;
            case EnemyBehaviour.MoveRight:
                target.MoveRight();
                break;
        }
    }

    public void BeginMoveLeft()
    {
        behaviour = EnemyBehaviour.MoveLeft;
    }  
    public void BeginMoveRight()
    {
        behaviour = EnemyBehaviour.MoveRight; 
    }  
    public void Jump()
    {
        controlledTarget.Jump();
    }


}
public enum EnemyBehaviour
{
    MoveLeft = 1,
    MoveRight = 2,
    Jump = 3
}
