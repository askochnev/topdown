﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public void SpawnEnemy(Character enemy)
    {
        var spawnedEnemy = Instantiate(enemy, transform.position, Quaternion.identity);
    }
}
