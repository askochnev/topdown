﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new EnemyWaveConfig", menuName = "EnemyWaveConfig", order = 51)]
public class EnemyWaveConfig : ScriptableObject 
{  
    [SerializeField]
    private List<EnemySpawnConfig> _spawnConfig;

    [SerializeField]
    private float _delayAfterWaveInSeconds;

    public float DelayAfterWave 
    {
        get
        {
            return _delayAfterWaveInSeconds;
        }
    }

    public EnemySpawnConfig GetSpawnConfigByWaveIndex(int enemyIndex)
    {
        //return enemyIndex < _spawnConfig.Count ? _spawnConfig[enemyIndex] : null;

        if (enemyIndex < _spawnConfig.Count)
        {
            return _spawnConfig[enemyIndex];
        }
        else
        {
            return null;
        }
    }
    public bool IsWaveFinished(int enemyIndex)
    {
        return enemyIndex >= _spawnConfig.Count;
    }
}

[System.Serializable]
public class EnemySpawnConfig
{
    public Character enemy;
    public float delayAferSpawnInSeconds = 1.0f;
}
