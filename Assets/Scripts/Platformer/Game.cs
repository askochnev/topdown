﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField]
    private LevelConfig _levelConfig;

    [SerializeField]
    private WaveController _waveController;

    void Start()
    {
        BeginPlay(_levelConfig);
    }

    private int _waveIndex = 0;
    private LevelConfig _level;
    public void BeginPlay(LevelConfig level)
    {
        _level = level;
        _waveIndex = 0;
        _waveController.onWaveFinished += StartNextWave;
        StartNextWave();
    }

    public void StartNextWave()
    {
        _waveIndex++;
        var nextWave = _level.GetWaveConfig(_waveIndex);
        _waveController.BeginWave(nextWave);
    }
}
