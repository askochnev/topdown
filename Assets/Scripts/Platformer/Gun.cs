﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField]
    private Bullet bulletPrefab;
    private Vector2 _direction;
    public void AimLeft()
    {
        _direction = Vector2.left;
    }
    public void AimRight()
    {
        _direction = Vector2.right;
    }

    public void Shoot()
    {
        var bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        bullet.SetDirection(_direction);
    }
}
