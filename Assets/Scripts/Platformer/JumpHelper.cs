﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpHelper : MonoBehaviour
{
    public int _jumps = 0;
    public const int MAX_JUMPS_AMOUNT = 2;
    public bool MayJump()    {
        return _jumps > 0;
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        _jumps = MAX_JUMPS_AMOUNT;
    }
    private void OnTriggerExit2D(Collider2D other)     {
        _jumps--;
    }
    public void OnJump()    {
        _jumps--;
    }
}
