﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "new LevelConfig", menuName = "LevelConfig", order = 0)]
public class LevelConfig : ScriptableObject 
{
    [SerializeField]
    private List<EnemyWaveConfig> _waves;

    public EnemyWaveConfig GetWaveConfig(int waveIndex)
    {
        //return waveIndex < _waves.Count ? _waves[waveIndex] : null;

        if (waveIndex < _waves.Count)
        {
            return _waves[waveIndex];
        }
        else
        {
            return null;
        }
    }

    public bool IsLevelFinished(int waveIndex)
    {
        return waveIndex >= _waves.Count;
    }
    
}