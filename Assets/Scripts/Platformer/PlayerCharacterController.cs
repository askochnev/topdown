﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterController : CharacterController
{
    public override void ProcessInput(Character target)
    {
            target.Stop();
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                target.MoveLeft();
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                target.MoveRight();
            }
            if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow))
            {
                target.Stop();
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                target.Jump();
            }
    }
}
