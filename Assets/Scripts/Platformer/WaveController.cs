﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour
{
    [SerializeField]
    private List<EnemySpawner> _spawners;

    public event System.Action onWaveFinished;

    public void BeginWave(EnemyWaveConfig waveConfig)
    {
        StartCoroutine(WaveRoutine(waveConfig));
    }

    private IEnumerator WaveRoutine(EnemyWaveConfig waveConfig)
    {
        int i = 0;
        while(!waveConfig.IsWaveFinished(i))
        {
            var nextEnemy = waveConfig.GetSpawnConfigByWaveIndex(i);
            GetNextSpawner(i).SpawnEnemy(nextEnemy.enemy);
            yield return new WaitForSeconds(nextEnemy.delayAferSpawnInSeconds);
            i++;
        }
        yield return new WaitForSeconds(waveConfig.DelayAfterWave);
        onWaveFinished?.Invoke();
    }

    public EnemySpawner GetNextSpawner(int spawnerIndex)
    {
        return _spawners[Random.Range(0, _spawners.Count)];
    }
}
