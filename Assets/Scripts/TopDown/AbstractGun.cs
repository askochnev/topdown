﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractGun : MonoBehaviour
{
    private float _reloadFinishTime;
    private float _delayFinishTime;

    [SerializeField]
    private float delayTime = 0.1f;

    [SerializeField]
    private float reloadTime = 1.2f;

    [SerializeField]
    private int shots = 10;

    [SerializeField]
    private Projectile _projectilePrefab;

    private int _shoots = 0;

    protected abstract void SingleShot(Projectile projectile, Vector3 spawnPosition, Vector3 targetPosition);

    private Inventory _linkedInventory;
    public void LinkInventory(Inventory inventory)
    {
        _linkedInventory = inventory;
    }

    [SerializeField]
    private AmmoType _ammoType = AmmoType.Infinite;
    public AmmoType AmmoType => _ammoType;

    public void Shoot(Vector3 targetPosition)
    {
        if (Time.time > _reloadFinishTime && Time.time > _delayFinishTime
            && _linkedInventory.GetAmmo(_ammoType) > 0)
        {
            SingleShot(_projectilePrefab, transform.position + transform.forward * 0.2f, targetPosition);
            _linkedInventory.UseAmmo(_ammoType);
            _shoots++;
            if (_shoots >= shots)
            {
                _reloadFinishTime = Time.time + reloadTime;
                _shoots = 0;
            }
            else
            {
                _delayFinishTime = Time.time + delayTime;
            }
        }
    }
}
