﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : CollectableItem
{
    [SerializeField]
    private AmmoType _ammoType;

    [SerializeField][Range(1, 100)]
    private int _amount;

    public override void Collect(Player player)
    {
        player.Inventory.AddAmmo(_ammoType, _amount);
        Destroy(this.gameObject);
    }
}

public enum AmmoType
{
    Infinite = 1,
    MachineGun = 2,
    ShotGun = 3
}
