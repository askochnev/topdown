﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    [SerializeField]
    private Transform _target;

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, _target.position) > 0.2f)
        {
            var nextPos = Vector3.Lerp(transform.position, _target.position, 0.5f);
            nextPos.y = transform.position.y;
            transform.position = nextPos;
        }
    }
}
