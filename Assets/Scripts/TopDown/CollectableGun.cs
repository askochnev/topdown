﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableGun : CollectableItem
{
    [SerializeField]
    private AbstractGun _gunToCollectPrefab;

    [SerializeField]
    private int _ammoAmount = 100;

    public override void Collect(Player player)
    {
        player.GunsHolder.AddGun(_gunToCollectPrefab, _ammoAmount);
    }
}
