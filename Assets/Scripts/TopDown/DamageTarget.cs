﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTarget : MonoBehaviour
{
    [SerializeField]
    private int _health = 100;

    public int MaxHealth => _maxHealth;
    private int _maxHealth; 

    [SerializeField]
    private MeshRenderer _meshRend;

    [SerializeField]
    [Range(1, 5)]
    private int _damageFlashHalfSteps = 5;

    [SerializeField]
    [Range(1, 5)]
    private int _damageFlashesAmount = 2;

    [SerializeField]
    private bool _isInvulnerable = false;

    private void Awake()
    {
        _maxHealth = _health;
    }

    private void Start()
    {
        GameController.Instance.RegiterDamageTarget(this);
    }

    public void GetDamage(int damage)
    {
        if (!_isInvulnerable)
        {
            _health -= damage;
        }
        if (_health <= 0)
        {
            Destroy(this.gameObject);
        }        
        else
        {
            StartCoroutine(DamageFlashRoutine());
        }
    }

    public float GetNormalizedValue()
    {
        return (float)_health / (float)MaxHealth;
    }


    private IEnumerator DamageFlashRoutine()
    {
        var material = _meshRend.material;
        for (var flashIndex = 0; flashIndex < _damageFlashesAmount; flashIndex++)
        {
            for (var i = 0; i <= _damageFlashHalfSteps; i++)
            {
                var flashIntensity = (float)i / (float)_damageFlashHalfSteps;
                material.SetFloat("_DamageEffectValue", flashIntensity);
                yield return null;
            }
            for (var i = _damageFlashHalfSteps; i >= 0; i--)
            {
                var flashIntensity = (float)i / (float)_damageFlashHalfSteps;
                material.SetFloat("_DamageEffectValue", flashIntensity);
                yield return null;
            }
        }
    }
}
