﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Inventory))]
public class InventoryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var inventory = target as Inventory;
        foreach (var ammo in inventory.Ammo)
        {
            EditorGUILayout.LabelField(ammo.Key + ":" + ammo.Value);
        }
        base.OnInspectorGUI();
    }
}
