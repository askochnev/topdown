﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [SerializeField]
    private Player _controlTarget;

    [SerializeField]
    private NavMeshAgent _agent;

    private Vector3 _targetPoint;
    public Vector3 TargetPoint => _targetPoint;

    public int Id { get; private set; }
    private static int idCounter = 0;

    public void SetTargetPoint(Vector3 newTarget)
    {
        _targetPoint = newTarget;
    }

    private void Start()
    {
        Id = idCounter++;
        GameController.Instance.Commander.RegisterBot(this);
    }

    private void OnDestroy()
    {
        GameController.Instance.Commander.UnregisterBot(this);
    }
    
    private void ChooseMoveTarget()
    {
        switch (_movementType)
        {
            case EnemyMovementType.Follow:
                _agent.destination = GameController.Instance.Player.transform.position;
                break;
            case EnemyMovementType.Random:
                _agent.destination = GetRandomPoint();
                break;
            case EnemyMovementType.WaypointsRandom:
                break;
            case EnemyMovementType.Stop:
                _agent.destination = transform.position;
                break;
            case EnemyMovementType.GoToTargetPoint:
                _agent.destination = _targetPoint;
                break;
        }
    }
    private Vector3 GetRandomPoint()
    {
        if (Vector3.Distance(transform.position, _destinationPosition) < 0.5f)
        {
            _destinationPosition = new Vector3(
                Random.Range(-50f, 50f),
                0,
                Random.Range(-50f, 50f));

        }
        return _destinationPosition;
    }
    private Vector3 GetRandomWaypoint()
    {
        if (Vector3.Distance(transform.position, _destinationPosition) < 0.5f)
        {
            if (_waypoints.Count == 0) {return GetRandomPoint(); }
            _destinationPosition = _waypoints[Random.Range(0, _waypoints.Count)].position;

        }
        return _destinationPosition;
    }

    [SerializeField]
    private List<Transform> _waypoints;
    private Vector3 _destinationPosition;

    private void Awake()
    {
        _destinationPosition = transform.position;
        Path = new NavMeshPath();
        //StartCoroutine(BotBehaviourRoutine());
    }

    private IEnumerator BotBehaviourRoutine()
    {
        var values = System.Enum.GetValues(typeof(EnemyMovementType));
        while (true)
        {
            _movementType = (EnemyMovementType)values.GetValue(Random.Range(0, values.Length));
            yield return new WaitForSeconds(Random.Range(5f,10f));
        }
    }

    [SerializeField]
    private EnemyMovementType _movementType = EnemyMovementType.Follow;

    public void SetMoveType(EnemyMovementType moveType)
    {
        _movementType = moveType;
    }

    public enum EnemyMovementType
    {
        Random = 1,
        Follow = 2,
        WaypointsRandom = 3,
        Stop = 5,
        GoToTargetPoint = 6
    }

    private void Aim()
    {
        _controlTarget.RotateTo(GameController.Instance.Player.transform.position);
    }

    private bool PlayerInShootRange()
    {
        var myPosition = transform.position + Vector3.up;
        var targetPosition = GameController.Instance.Player.transform.position;
        var direction = targetPosition - myPosition;
        var raycastDist = 10f;
        RaycastHit hit;
        if(Physics.Raycast(myPosition, direction, out hit, raycastDist))
        {
            return hit.collider.gameObject.CompareTag("Player");
        }
        return false;
    }

    private void Shoot()
    {
        if (PlayerInShootRange())
        {
            _controlTarget.Shoot();
        }
    }






    public NavMeshPath Path { get; private set; }
    private int cachedIndex;

    private void Update()
    {
        Aim();
        Shoot();
        ChooseMoveTarget();
    }

    public Vector3 GetNearestPointToTarget(Vector3 target)
    {
        Path.ClearCorners();
        _agent.CalculatePath(target, Path);
        for (var i = 0; i < Path.corners.Length; i++)
        {
            if (Vector3.Distance(transform.position, Path.corners[i]) > 2f)
            {
                cachedIndex = i;
                return Path.corners[i];
            }
        }
        return transform.position;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        if (Path != null)
        {
            for (var i = 0; i < Path.corners.Length - 1; i++)
            {
                Gizmos.DrawLine(Path.corners[i], Path.corners[i + 1]);
                Gizmos.DrawSphere(Path.corners[i], 0.3f);
            }
            if (Path.corners.Length > 1)
            {
                Gizmos.DrawSphere(Path.corners[cachedIndex], 1.5f);
            }
        }
    }
}
