﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPointer : MonoBehaviour
{
    [SerializeField]
    private Transform _trackedTarget = null;

    private DamageTarget _dt;

    [SerializeField]
    private ProgressBar _progressBar;

    private void Awake()
    {
        if(_trackedTarget != null && _dt == null)
        {
            _dt = _trackedTarget.gameObject.GetComponent<DamageTarget>();
        }
    }

    public void SetTarget(Player player)
    {
        _trackedTarget = player.transform;
        _dt = player.gameObject.GetComponent<DamageTarget>();
    }

    public void SetTarget(DamageTarget target)
    {
        _dt = target;
        _trackedTarget = target.transform;
    }

    private void Update()
    {
        if (_trackedTarget != null)
        {
            var rt = this.GetComponent<RectTransform>();
            transform.position = Camera.main.WorldToScreenPoint(_trackedTarget.position);
            _progressBar.SetValue(_dt.GetNormalizedValue());
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
