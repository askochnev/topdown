﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Player _player;
    public Player Player => _player;

    [SerializeField]
    private NavigationAgentsCommander _commander;
    public NavigationAgentsCommander Commander => _commander;

    [SerializeField]
    private Canvas _canvas;

    [SerializeField]
    private EnemyPointer _pointer;

    public static GameController Instance { get; private set; }


    private void Awake()
    {
        Instance = this;
    }

    public void RegiterDamageTarget(DamageTarget dtarget)
    {
        var pointer = Instantiate(_pointer, _canvas.transform);
        pointer.SetTarget(dtarget);
    }
}
