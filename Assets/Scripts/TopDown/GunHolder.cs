﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GunHolder : MonoBehaviour
{
    [SerializeField]
    private List<AbstractGun> _guns = new List<AbstractGun>();

    private Inventory _linkedInventory;
    public void LinkInventory(Inventory inventory)
    {
        _linkedInventory = inventory;
    }

    private int _activeGunIndex = 0;


    private bool IsSameGun(AbstractGun gun1, AbstractGun gun2)
    {
        return gun1.AmmoType == gun2.AmmoType;
    }

    public void AddGun(AbstractGun gunPrefab, int ammoToAdd = 100)
    {
        foreach (var playerGun in _guns)
        {
            if (IsSameGun(playerGun, gunPrefab))
            {
                _linkedInventory.AddAmmo(gunPrefab.AmmoType, ammoToAdd);
                return;
            }
        }

        var gun = Instantiate(gunPrefab);

        _guns.Add(gun);
        gun.transform.parent = this.transform;
        gun.transform.localPosition = Vector3.zero;
        gun.transform.localRotation = Quaternion.identity;
        ActiveGun.gameObject.SetActive(false);
        _activeGunIndex = _guns.Count - 1;
        ActiveGun.gameObject.SetActive(true);
        _linkedInventory.AddAmmo(gunPrefab.AmmoType, ammoToAdd);
    }

    public AbstractGun ActiveGun
    {
        get
        {
            if (_activeGunIndex >= 0 && _activeGunIndex < _guns.Count)
            {
_guns[_activeGunIndex].LinkInventory(_linkedInventory);
                return _guns[_activeGunIndex];
            }
            throw new ArgumentException();
        }
    }

    public void NextGun()
    {
        ActiveGun.gameObject.SetActive(false);
        _activeGunIndex = (_activeGunIndex + 1) % _guns.Count;
        ActiveGun.gameObject.SetActive(true);
    }
}
