﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private Dictionary<AmmoType, int> _ammo = new Dictionary<AmmoType, int>();
    public Dictionary<AmmoType, int> Ammo => _ammo;

    [SerializeField]
    private List<DefaultAmmo> _defaultAmmo;

    private void CheckAmmo(AmmoType type)
    {
        if (!_ammo.ContainsKey(type))
        {
            _ammo.Add(type, GetDefaultAmmo(type));
        }
    }

    private int GetDefaultAmmo(AmmoType type)
    {
        for (var i = 0; i < _defaultAmmo.Count; i++)
        {
            if (_defaultAmmo[i].type == type)
            {
                return _defaultAmmo[i].amount;
            }
        }
        return 0;
    }

    [System.Serializable]
    private struct DefaultAmmo
    {
        public AmmoType type;
        public int amount;
    }

    public int GetAmmo(AmmoType type)
    {
        if (type == AmmoType.Infinite)
        {
            return int.MaxValue;
        }
        CheckAmmo(type);
        return _ammo[type];
    }

    public void AddAmmo(AmmoType type, int amount)
    {
        CheckAmmo(type);
        _ammo[type] += amount;
    }
    public void UseAmmo(AmmoType type, int amount = 1)
    {
        CheckAmmo(type);
        _ammo[type] -= amount;
    }
}
