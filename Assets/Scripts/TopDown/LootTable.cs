﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootTable : MonoBehaviour
{
    [SerializeField]
    private List<CollectableItem> _loot;

    public void DropLoot()
    {
        Instantiate(_loot[Random.Range(0, _loot.Count)], 
            transform.position, Quaternion.identity);
    }

    private void OnDestroy()
    {
        DropLoot();
    }
}
