﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : AbstractGun
{ 
    protected override void SingleShot(Projectile projectilePrefab, Vector3 spawnPosition, Vector3 targetPosition)
    {
        var projectile = Instantiate(projectilePrefab, spawnPosition, Quaternion.identity);
        projectile.FlyTo(targetPosition + Scatter());
        projectile.tag = gameObject.tag;
    }

    [Range(0,1)]
    public float scatter = 0.3f;

    private Vector3 Scatter()
    {
        return new Vector3 (Random.Range(-scatter, scatter), 0, Random.Range(-scatter, scatter));
    }
}
