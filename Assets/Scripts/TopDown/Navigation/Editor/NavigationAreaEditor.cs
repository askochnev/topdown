﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NavigationArea))]
public class NavigationAreaEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var area = target as NavigationArea;
        if (GUILayout.Button("Build Navigaton Area"))
        {
            area.BuildArea();
        }
    }

    // добавить в NavigationArea
    private Color GetColor(NavigationPoint point)
    {
        if (!point.isPassable)
        {
            return Color.black;
        }
        if (point.weight >= 100000)
        {
            return Color.magenta;
        }
        return Color.white;
    }
    //в OnDrawGizmos
    //var color = GetColor(_navPoints[i * height + j]);

    //добавить в самом начале OnDrawGizmos, после return;:
    // UpdatePlayerVisibilityWeights();
}
