﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationAgentsCommander : MonoBehaviour
{
    [SerializeField]
    private NavigationArea _area;

    private List<EnemyController> _bots = new List<EnemyController>();

    [SerializeField]
    [Range(1f, 100f)]
    private float _botsPositionDistanceCoeff = 20f;

    [SerializeField]
    [Range(0f, 5f)]
    private float _botsMovementCost = 1f;

    [SerializeField]
    [Range(0f, 5f)]
    private float _distanceToPlayerCost = 1f;
    public float DistanceToPlayerCost => _distanceToPlayerCost;

    public int NavigationAgentsTick { get; private set; }


    public void RegisterBot(EnemyController bot)
    {
        _bots.Add(bot);
        bot.SetMoveType
            (EnemyController.EnemyMovementType.GoToTargetPoint);
    }

    public void UnregisterBot(EnemyController bot)
    {
        _bots.Remove(bot);
    }

    private void Start()
    {
        NavigationAgentsTick = 0;
        StartCoroutine(UpdateNavigationPointsRoutine());
    }

    public IEnumerator UpdateNavigationPointsRoutine()
    {
        while(true)
        {
            for (var i = 0; i < _bots.Count; i++)
            {
                var bot = _bots[i];
                var point = _area.FindNearestSafePoint(bot, AdditionalCost);
                bot.SetTargetPoint(point);
                yield return null;
            }

            /*if (AllBotsReachedTargets())
            {
                for (var i = 0; i < _bots.Count; i++)
                {
                    _bots[i].SetMoveType
                        (EnemyController.EnemyMovementType.Follow);
                }
            }*/
            yield return null;
            NavigationAgentsTick++;
        }
    }

    private bool AllBotsReachedTargets()
    {
        var reachedRadius = 2.5f;
        for (var i = 0; i < _bots.Count; i++)
        {
            if (Vector3.Distance(_bots[i].transform.position,
                _bots[i].TargetPoint) > reachedRadius)
            {
                return false;
            }
        }

        return true;
    }

    [SerializeField][Range(1f, 25f)]
    private float _minDistanceBetweenBots = 5f;

    public float AdditionalCost(EnemyController currentBot, NavigationPoint point)
    {
        var cost = 0f;
        for (var i = 0; i < _bots.Count; i++)
        {
            if (_bots[i].Id != currentBot.Id)
            {
                var dist = Vector3.Distance(_bots[i].TargetPoint, point.position);
                if (dist < _minDistanceBetweenBots)
                {
                    cost += 100f * (_minDistanceBetweenBots - dist) / _minDistanceBetweenBots;
                }
            }
        }

        return cost;
    }

    private void OnDrawGizmos()
    {
        foreach (var bot in _bots)
        {
            var point = bot.TargetPoint;
            var color = Color.red;
            color.a = 0.5f;
            Gizmos.color = color;
            Gizmos.DrawSphere(point, 2f);

            GizmosDrawSearchArea(bot.transform);
        }
    }

    private void GizmosDrawSearchArea(Transform bot)
    {
        Gizmos.color = Color.black;
        var lookDist = _area.BotComputeDistanceLimit * _area.MeshStep;
        
        Gizmos.DrawLine(bot.transform.position + new Vector3(lookDist, 0, lookDist), bot.transform.position + new Vector3(lookDist, 0, -lookDist));
        Gizmos.DrawLine(bot.transform.position + new Vector3(lookDist, 0, -lookDist), bot.transform.position + new Vector3(-lookDist, 0, -lookDist));
        Gizmos.DrawLine(bot.transform.position + new Vector3(-lookDist, 0, -lookDist), bot.transform.position + new Vector3(-lookDist, 0, lookDist));
        Gizmos.DrawLine(bot.transform.position + new Vector3(-lookDist, 0, lookDist), bot.transform.position + new Vector3(lookDist, 0, lookDist));
    }
}
