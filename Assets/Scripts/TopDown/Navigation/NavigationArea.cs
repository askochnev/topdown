﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationArea : MonoBehaviour
{
    [SerializeField]
    [Range(0, 1000)]
    private int _areaWidth = 5;

    [SerializeField]
    [Range(0, 1000)]
    private int _areaHeight = 5;

    [SerializeField]
    [Range(0.5f, 3f)]
    private float _meshStep = 1f;
    public float MeshStep => _meshStep;

    [SerializeField][HideInInspector]
    private List<NavigationPoint> _navPoints = new List<NavigationPoint>();

    [SerializeField]
    private int width;
    [SerializeField]
    private int height;

    public void BuildArea()
    {
        width = (int)(_areaWidth / _meshStep);
        height = (int)(_areaHeight / _meshStep);
        _navPoints.Clear();

        for (var i = 0; i < width; i++)
        {
            for (var j = 0; j < height; j++)
            {
                var x = -_areaWidth / 2f + i * _meshStep + _meshStep * 0.5f;
                var z = -_areaHeight / 2f + j * _meshStep + _meshStep * 0.5f;
                _navPoints.Add(new NavigationPoint());
                _navPoints[i * height + j].position 
                    = new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z);

                _navPoints[i * height + j].isPassable = CheckPassability(_navPoints[i * height + j]);

                _navPoints[i * height + j].index = i * height + j;
            }
        }
        Debug.Log(this.GetType().ToString());
    }


    private bool CheckPassability(NavigationPoint point)
    {
        var pointPosition = point.position;
        var castRadius = _meshStep * 0.5f;
        var distance = 1f;
        //var layerMask = gameObject.layer;

        var hits = Physics.SphereCastAll(pointPosition, castRadius, Vector3.up, distance);
        var isPassable = true;
        foreach (var hit in hits)
        {
            if (IsObstacle(hit.collider.gameObject))
            {
                isPassable = false;
            }
        }
        return isPassable;
    }

    private bool IsObstacle(GameObject go)
    {
        return go.name.Contains("Obstacle");
    }

    private void UpdatePlayerVisibilityWeights()
    {
        for (var i = 0; i < width; i++)
        {
            for (var j = 0; j < height; j++)
            {
                var index = i * height + j;
                if (_navPoints[index].isPassable)
                {
                    if (PointIsVisibleFromPlayerPosition(_navPoints[index]))
                    {
                        _navPoints[index].weight = 100000;
                    }
                    else
                    {
                        _navPoints[index].weight = 0;
                    }
                }
            }
        }
    }
    [SerializeField]
    private Transform _player;

    private bool PointIsVisibleFromPlayerPosition(NavigationPoint navPoint)
    {
        if (navPoint.cacheTick == GameController.Instance.Commander.NavigationAgentsTick)
        {
            return navPoint.isVisibleByPlayer;
        }

        var origin = navPoint.position + Vector3.up * 2f;
        var playerPos = _player.transform.position;
        var distanceFromPlayerToPoint = Vector3.Distance(playerPos, origin);
        RaycastHit hit;
        var direction = playerPos - origin;

        navPoint.isVisibleByPlayer = false;

        if (Physics.Raycast(origin, direction, out hit, distanceFromPlayerToPoint))
        {
            if (IsPlayerHit(hit))
            {
                navPoint.isVisibleByPlayer = true;
            }
        }
        navPoint.cacheTick = GameController.Instance.Commander.NavigationAgentsTick;

        return navPoint.isVisibleByPlayer;
    }

    private bool IsPlayerHit(RaycastHit hit)
    {
        return hit.collider.transform == _player || 
            (hit.collider.transform.parent != null 
            && hit.collider.transform.parent.parent != null 
            && hit.collider.transform.parent.parent == _player);
    }

    public delegate float AdditionalCostDelegate(EnemyController bot, NavigationPoint point);

    [SerializeField]
    private int _botComputeDistanceLimit = 10;
    public int BotComputeDistanceLimit => _botComputeDistanceLimit;

    public Vector3 FindNearestSafePoint(EnemyController bot, AdditionalCostDelegate additionalCostDelegate)
    {
        var minCost = float.MaxValue;
        var minIndex = 0;

        var agentNearestPoint = GetNearestNavigationPoint(bot.transform.position);
        var nextPathPoint = bot.GetNearestPointToTarget(_player.position);

        for (var i = -_botComputeDistanceLimit; i <= _botComputeDistanceLimit; i++)
        {
            for(var j = -_botComputeDistanceLimit; j <= _botComputeDistanceLimit; j++)
            {
                var index = agentNearestPoint.index + i * height + j;
                if (index >= 0 && index < _navPoints.Count 
                    && agentNearestPoint.index % height + j >= 0 
                    && agentNearestPoint.index % height + j < height && _navPoints[index].isPassable)
                {
                    var cost = EvaluateCost(_navPoints[index], bot, nextPathPoint);

                    if (cost < minCost)
                    {
                        cost += ComputeCost(_navPoints[index], bot, additionalCostDelegate);
                        if (cost < minCost)
                        {
                            minCost = cost;
                            minIndex = index;
                        }
                    }
                }
            }
        }
        return _navPoints[minIndex].position;
    }

    private float EvaluateCost(NavigationPoint navPoint, EnemyController bot, Vector3 nextPathPoint)
    {
        return 1f * Vector3.Distance(navPoint.position, bot.transform.position)
               + 3f * Vector3.Distance(navPoint.position, nextPathPoint)
               + 0.5f * Vector3.Distance(navPoint.position, _player.position);
    }

    private float ComputeCost(NavigationPoint navPoint, EnemyController bot, AdditionalCostDelegate additionalCostDelegate)
    {
        var visibilityCost = 0f;
        if (PointIsVisibleFromPlayerPosition(navPoint))
        {
            visibilityCost = 10000f;
        }

        var additionalCost = 0f;
        if (additionalCostDelegate != null)
        {
            additionalCost = additionalCostDelegate.Invoke(bot, navPoint);
        }

        return visibilityCost + additionalCost;
    }

    private int DistanceToPlayerCost(NavigationPoint navPoint)
    {
        return Mathf.RoundToInt(Vector3.Distance(_player.position, navPoint.position)
            * GameController.Instance.Commander.DistanceToPlayerCost);
    }
    
    public NavigationPoint GetNearestNavigationPoint(Vector3 point)
    {
        var i = Mathf.RoundToInt((point.x - transform.position.x + _areaWidth / 2f) / _meshStep);
        var j = Mathf.RoundToInt((point.z - transform.position.z + _areaHeight / 2f) / _meshStep);

        var index = i * height + j;
        if (index < 0 || index >= _navPoints.Count)
        {
            var minValue = float.MaxValue;
            for (var k = 0; k < _navPoints.Count; k++)
            {
                if (_navPoints[k].isPassable)
                {
                    var dist = Vector3.Distance(_navPoints[k].position, point);
                    if (dist < minValue)
                    {
                        minValue = dist;
                        index = k;
                    }
                }
            }
        }
        if (!_navPoints[index].isPassable)
        {
            while (!_navPoints[index].isPassable)
            {
                index = (index + 1) % _navPoints.Count;
            }
        }
        Debug.DrawLine(_navPoints[index].position, _navPoints[index].position + Vector3.up * 10, Color.black);
        return _navPoints[index];
    }

    private Color GetColor(NavigationPoint point)
    {
        if (!point.isPassable)
        {
            return Color.black;
        }
        if (point.weight >= 100000)
        {
            return Color.magenta;
        }
        return Color.white;
    }

    private void OnDrawGizmos()
    {
        GizmoRaycastCheck();
    }

    private void GizmoRaycastCheck()
    {
        var magenta = Color.magenta;
        magenta.a = 0.25f;
        var origin = _player.position;
        for (var i = 0; i < 360; i++)
        {
            var direction = new Vector3(Mathf.Sin(i), 0, Mathf.Cos(i));
            RaycastHit hit;
            var raycastRadius = 100f;
            if (Physics.Raycast(origin, direction, out hit, raycastRadius))
            {
                raycastRadius = hit.distance;
            }
            Debug.DrawRay(origin, direction * raycastRadius, magenta);
        }
    }

}

[System.Serializable]
public class NavigationPoint
{
    public Vector3 position;
    public bool isPassable;

    public bool isVisibleByPlayer = false;
    public int cacheTick = 0;
    public int index;


    public int weight;
}