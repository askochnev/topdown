﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float maxMovingSpeed;   

    private Vector3 _aimDirection;

    [SerializeField]
    private GunHolder _gunHolder;
    public GunHolder GunsHolder => _gunHolder;

    [SerializeField]
    private Inventory _inventory;
    public Inventory Inventory => _inventory;

    [SerializeField]
    private Rigidbody _rb;

    private void Awake()
    {
        _gunHolder.LinkInventory(_inventory);
    }

    public void RotateTo(Vector3 targetPoint)
    {
        targetPoint.y = transform.position.y;
        _aimDirection = targetPoint - transform.position;
        transform.LookAt(targetPoint);
    }

    public void Shoot()
    {
        _gunHolder.ActiveGun.Shoot(transform.position + _aimDirection);
    }

    public void NextGun()
    {
        _gunHolder.NextGun();
    }

    public void MoveTo(Vector3 shift)
    {
        _rb.MovePosition(transform.position + shift.normalized * maxMovingSpeed);
        _rb.velocity = Vector3.zero;
    }

    public void OnCollisionEnter(Collision collision)
    {
        var item = collision.gameObject.GetComponent<CollectableItem>();
        if (item != null)
        {
            item.Collect(this);
            Destroy(item.gameObject);
        }
    }
}
