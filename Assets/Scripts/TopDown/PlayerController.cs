﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Player _controlTarget;

    private void Update()
    {
        Aim();
        Shoot();
        Move();
        CheckGuns();
    }

    private void CheckGuns()
    {
        if (NextGun())
        {
            _controlTarget.NextGun();
        }
        if (PrevGun())
        {
            _controlTarget.NextGun();
        }
    }

    private void Aim()
    {
        var mp = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 100);
        var position = Camera.main.ScreenToWorldPoint(mp);
        _controlTarget.RotateTo(position);
    }

    private void Shoot()
    {
        if (ShootPressed())
        {
            _controlTarget.Shoot();
        }
    }

    private void Move()
    {
        var shift = Vector3.zero;
        if (LeftPressed())
        {
            shift.x -= 1;
        }
        if (RightPressed())
        {
            shift.x += 1;
        }
        if (DownPressed())
        {
            shift.z -= 1;
        }
        if (UpPressed())
        {
            shift.z += 1;
        }
        _controlTarget.MoveTo(shift);
    }

    private bool LeftPressed()
    {
        return Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A);
    }

    private bool DownPressed()
    {
        return Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S);
    }

    private bool RightPressed()
    {
        return Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D);
    }

    private bool UpPressed()
    {
        return Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W);
    }

    private bool ShootPressed()
    {
        return Input.GetMouseButton(0);
    }

    public bool NextGun()
    {
        return Input.mouseScrollDelta.y > 0f;
    }

    public bool PrevGun()
    {
        return Input.mouseScrollDelta.y < 0f;
    }
}
