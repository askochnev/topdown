﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BFS : MonoBehaviour
{
    private bool[,] isVisited;
    [SerializeField]
    private BFSNode _bfsPrefab;
    private List<BFSNode> allNodes;

    public void StartSearch(TileType [,] map, int startX, int startY)
    {
        allNodes = new List<BFSNode>();
        isVisited = new bool[map.GetLength(0), map.GetLength(1)];
        for(var i = 0; i < map.GetLength(0); i++)
        {
            for(var j = 0; j < map.GetLength(1); j++)
            {
                isVisited[i, j] = false;
            }
        }
        //yield return new WaitForSeconds(0.1f);
        Queue<Node> queue = new Queue<Node>();
        queue.Enqueue(new Node(startX, startY, 0));//поместить в очередь
        isVisited[startX, startY] = true;
        int maxDepth = 0;
        while(queue.Count > 0)
        {
            var currentNode = queue.Dequeue();//достать первый элемент из очереди
            if (currentNode.depth > maxDepth)
            {
                maxDepth = currentNode.depth;
            }
            var obj = Instantiate(_bfsPrefab, transform);
            obj.SetNode(currentNode);
            allNodes.Add(obj);

            if (IsCorrectNode(currentNode.i + 1, currentNode.j, map))            {
                queue.Enqueue(new Node(currentNode.i + 1, currentNode.j, currentNode.depth + 1));
                isVisited[currentNode.i + 1, currentNode.j] = true;
                //yield return new WaitForSeconds(0.1f);
            }
            if (IsCorrectNode(currentNode.i - 1, currentNode.j, map))            {
                queue.Enqueue(new Node(currentNode.i - 1, currentNode.j, currentNode.depth + 1));
                isVisited[currentNode.i - 1, currentNode.j] = true;
               // yield return new WaitForSeconds(0.1f);
            }
            if (IsCorrectNode(currentNode.i, currentNode.j + 1, map))            {
                queue.Enqueue(new Node(currentNode.i, currentNode.j + 1, currentNode.depth + 1));
                isVisited[currentNode.i, currentNode.j + 1] = true;
                //yield return new WaitForSeconds(0.1f);
            }
            if (IsCorrectNode(currentNode.i, currentNode.j - 1, map))            {
                queue.Enqueue(new Node(currentNode.i, currentNode.j - 1, currentNode.depth + 1));
                isVisited[currentNode.i, currentNode.j - 1] = true;
                //yield return new WaitForSeconds(0.1f);
            }
        }
        foreach(var node in allNodes)
        {
            node.Paint(maxDepth);
        }
    }

    private bool IsCorrectNode(int i, int j, TileType[,] map)
    {
        if (i < 0
            || j < 0
            || i >= isVisited.GetLength(0)
            || j >= isVisited.GetLength(1))
        {
            return false;
        }
        if(isVisited[i,j] || map[i,j] == TileType.Wall)
        {
            return false;
        }
        return true;
    }

    public struct Node
    {
        public Node(int i, int j, int depth)
        {
            this.i = i;
            this.j = j;
            this.depth = depth;
        }
        public int i;
        public int j;

        public int depth;
    }
}
