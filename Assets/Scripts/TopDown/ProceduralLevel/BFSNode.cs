﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BFSNode : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer _meshRend;
    private BFS.Node _node;

    public void SetNode(BFS.Node node)
    {
        _node = node;
        transform.localPosition = new Vector3(node.i, 0, node.j);
    }

    public void Paint(int maxDepth)
    {
        _meshRend.material.color = GetColor(_node.depth, maxDepth);
    }

    private Color GetColor(int currentDepth, int maxDepth)
    {
        var t = Mathf.Clamp((float)currentDepth / (float)maxDepth, 0, 1);
        return new Color(t, 1f - t, 0f);
    }
}
