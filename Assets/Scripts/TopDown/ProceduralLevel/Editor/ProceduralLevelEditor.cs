﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ProceduralLevel))]
public class ProceduralLevelEditor : Editor
{
    private int startX = 1;
    private int startY = 1;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var proceduralLevel = target as ProceduralLevel;
        if (GUILayout.Button("Generate"))
        {
            proceduralLevel.GenerateLevel();
        }

        if (GUILayout.Button("Clear"))
        {
            proceduralLevel.ClearLevel();
        }
        //if (Application.isPlaying)
        {
            startX = EditorGUILayout.IntSlider(startX, 0, proceduralLevel.LevelSize);
            startY = EditorGUILayout.IntSlider(startY, 0, proceduralLevel.LevelSize);
            if (GUILayout.Button("BeginBFS"))
            {
                proceduralLevel.RunBFS(startX, startY);
            }
            Debug.DrawLine(new Vector3(startX, 0, startY), new Vector3(startX, 1, startY));
        }
    }
}
