﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralLevel : MonoBehaviour
{
    [SerializeField]
    private GameObject _wallPrefab;

    [SerializeField]
    [Range(20, 200)]
    private int levelSize;
    public int LevelSize => levelSize;

    [SerializeField]
    [Range(0, 5)]
    private int deepness;

    private TileType[,] _map;

    [SerializeField]
    private BFS _bfs;

    public void RunBFS(int startX, int startY)
    {
        _bfs.StartSearch(_map, startX, startY);
        //StartCoroutine(_bfs.StartSearch(_map, startX, startY));
    }

    private void InitializeMap(int size)
    {
        _map = new TileType[size + 1, size + 1];
        for (var i = 0; i <= size; i++)
        {
            for (var j = 0; j <= size; j++)
            {
                _map[i, j] = TileType.Empty;
            }
        }
    }

    public void GenerateLevel()
    {
        ClearLevel();
        InitializeMap(levelSize);
        ProceduralIteration(0, 0, levelSize, levelSize, deepness);
        GenerateWalls();
    }

    public void ClearLevel()
    {
        var allBFSNodes = GetComponentsInChildren<BFSNode>();
        foreach (var child in allBFSNodes)
        {
            DestroyImmediate(child.gameObject);
        }

        var allChilds = GetComponentsInChildren<Transform>();
        foreach (var child in allChilds)
        {
            if (child != this.transform)
            {
                DestroyImmediate(child.gameObject);
            }
        }
    }

    void ProceduralIteration(int minX, int minY, int maxX, int maxY, int deep)
    {
        if (deep == 0)
        {
            PlaceRoom(minX, minY, maxX, maxY);
            return;
        }
        bool isVerticalSeparation = Random.Range(0, 2) == 1;

        if (isVerticalSeparation && maxY - minY > 3)
        {
            var separatorY = GetSeparator(minY, maxY);
            ProceduralIteration(minX, minY, maxX, separatorY, deep - 1);
            ProceduralIteration(minX, separatorY, maxX, maxY, deep - 1);
            PlaceDoorY(minX, maxX, separatorY);
        }
        else if (maxX - minX > 3)
        {
            var separatorX = GetSeparator(minX, maxX);
            ProceduralIteration(minX, minY, separatorX, maxY, deep - 1);
            ProceduralIteration(separatorX, minY, maxX, maxY, deep - 1);
            PlaceDoorX(minY, maxY, separatorX);
        }
        else
        {
            PlaceRoom(minX, minY, maxX, maxY);
        }
    }

    private int GetSeparator(int min, int max)
    {
        //return (max + min) / 2;
        if(min + 2 >= max - 1)
        {
            return (max + min) / 2;
        }
        return Random.Range(min + 2, max - 1);
    }

    private void PlaceDoorY(int minX, int maxX, int separatorY)
    {
        int doorPos;
        int iterations = 0;
        do
        {
            doorPos = Random.Range(minX, maxX + 1);
            if (iterations++ > 100) return;
        } while (!IsCorrectDoorPosition(doorPos, separatorY));
        _map[doorPos, separatorY] = TileType.Door;
    }

    private void PlaceDoorX(int minY, int maxY, int separatorX)
    {
        int doorPos;
        int iterations = 0;
        do
        {
            doorPos = Random.Range(minY, maxY + 1);
            if (iterations++ > 100) return;
        } while (!IsCorrectDoorPosition(separatorX, doorPos));
        _map[separatorX, doorPos] = TileType.Door;
    }

    private bool IsCorrectDoorPosition(int i, int j)
    {
        if (i <= 0 || j <= 0 || i >= levelSize || j >= levelSize) return false;
        int wallsAround = 0;
        if (_map[i + 1, j] == TileType.Wall) wallsAround++;
        if (_map[i - 1, j] == TileType.Wall) wallsAround++;
        if (_map[i, j + 1] == TileType.Wall) wallsAround++;
        if (_map[i, j - 1] == TileType.Wall) wallsAround++;
        if(wallsAround == 2)
        {
            return true;
        }
        return false;
    }


    public void PlaceRoom(int minX, int minY, int maxX, int maxY)
    {
        for (var i = minX; i <= maxX; i++)        {
            _map[i, minY] = TileType.Wall;
            _map[i, maxY] = TileType.Wall;        }
        for (var i = minY; i <= maxY; i++)        {
            _map[minX, i] = TileType.Wall;
            _map[maxX, i] = TileType.Wall;        }
    }

    private void GenerateWalls()
    {
        for (var i = 0; i <= levelSize; i++)        {
            for(var j = 0; j <= levelSize; j++)            {
                if(_map[i,j] == TileType.Wall)                {
                    GenerateWall(i, j);
                }
            }
        }
    }

    private void GenerateWall(int i, int j)    {
        var spawnedWall = Instantiate(_wallPrefab, transform);
        spawnedWall.transform.localPosition = new Vector3(i, 0, j);
    }
}

public enum TileType
{
    Empty = 1,
    Wall = 2,
    Door = 3
}
