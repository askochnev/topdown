﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRecursion : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab;
    private void Start()
    {
        StartCoroutine(RecursiveFunction(0));
    }

    public IEnumerator RecursiveFunction(int deepness)
    {
        var obj = Instantiate(prefab, transform);
        obj.transform.position = new Vector3(0, deepness, 0);

        yield return new WaitForSeconds(0.05f);
        StartCoroutine(RecursiveFunction(deepness + 1));
        StartCoroutine(RecursiveFunction(deepness - 1));
    }
}
