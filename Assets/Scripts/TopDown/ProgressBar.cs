﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField]
    private Image _bar;

    public void SetValue(float value)
    {
        value = Mathf.Clamp01(value);
        var scale = _bar.transform.localScale;
        scale.x = value;
        _bar.transform.localScale = scale;
    }
}
