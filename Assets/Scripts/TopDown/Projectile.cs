﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField]
    private int _damage = 1;

    [SerializeField]
    private Rigidbody _rb;

    [SerializeField]
    private float _flySpeed = 10;

    private Vector3 _velocity;

    public void FlyTo(Vector3 target)
    {
        _velocity = (target - transform.position).normalized *  _flySpeed;
    }

    public void FlyTo(Vector3 target, float velocity)
    {
        _velocity = (target - transform.position).normalized * velocity;
    }

    private void Update()
    {
        _rb.velocity = _velocity;
    }

    private void OnTriggerEnter(Collider other)
    {
        var damageTarget = other.GetComponent<DamageTarget>();
        if (damageTarget != null && !other.gameObject.CompareTag(gameObject.tag))
        {
            damageTarget.GetDamage(_damage);
        }
        Destroy(this.gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
