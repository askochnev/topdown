﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGun : AbstractGun
{
    protected override void SingleShot(Projectile projectilePrefab, Vector3 spawnPosition, Vector3 targetPosition)
    {
        for (var i = 0; i < 6; i++)
        {
            var projectile = Instantiate(projectilePrefab, spawnPosition, Quaternion.identity);
            projectile.FlyTo(targetPosition + Scatter(), Random.Range(8f, 12f));
            projectile.tag = gameObject.tag;
        }
    }

    [Range(0.1f, 5)]
    public float scatter = 1f;

    private Vector3 Scatter()
    {
        return new Vector3(Random.Range(-scatter, scatter), 0, Random.Range(-scatter, scatter));
    }
}
